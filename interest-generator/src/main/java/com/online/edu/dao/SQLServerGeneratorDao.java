package com.online.edu.dao;

import org.apache.ibatis.annotations.Mapper;

/**
 * SQLServer代码生成器
 *
 * @author renbin
 */
@Mapper
public interface SQLServerGeneratorDao extends GeneratorDao {

}
