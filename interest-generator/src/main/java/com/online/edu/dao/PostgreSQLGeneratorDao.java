package com.online.edu.dao;

import org.apache.ibatis.annotations.Mapper;

/**
 * PostgreSQL代码生成器
 *
 * @author renbin
 */
@Mapper
public interface PostgreSQLGeneratorDao extends GeneratorDao {

}
