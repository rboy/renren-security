package com.online.edu.dao;

import org.apache.ibatis.annotations.Mapper;

/**
 * Oracle代码生成器
 *
 * @author renbin
 */
@Mapper
public interface OracleGeneratorDao extends GeneratorDao {

}
