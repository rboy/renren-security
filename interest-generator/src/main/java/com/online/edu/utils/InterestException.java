package com.online.edu.utils;

/**
 * 自定义异常
 *
 * @author renbin
 */
public class InterestException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	
    private String msg;
    private int code = 500;
    
    public InterestException(String msg) {
		super(msg);
		this.msg = msg;
	}
	
	public InterestException(String msg, Throwable e) {
		super(msg, e);
		this.msg = msg;
	}
	
	public InterestException(String msg, int code) {
		super(msg);
		this.msg = msg;
		this.code = code;
	}
	
	public InterestException(String msg, int code, Throwable e) {
		super(msg, e);
		this.msg = msg;
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}
	
	
}
