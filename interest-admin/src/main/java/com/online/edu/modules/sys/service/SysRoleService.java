package com.online.edu.modules.sys.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.online.edu.common.utils.PageUtils;
import com.online.edu.modules.sys.entity.SysRoleEntity;

import java.util.Map;


/**
 * 角色
 *
 * @author renbin
 */
public interface SysRoleService extends IService<SysRoleEntity> {

	PageUtils queryPage(Map<String, Object> params);

	void saveRole(SysRoleEntity role);

	void update(SysRoleEntity role);
	
	void deleteBatch(Long[] roleIds);

}
