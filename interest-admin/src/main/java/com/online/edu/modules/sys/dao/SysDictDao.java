package com.online.edu.modules.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.online.edu.modules.sys.entity.SysDictEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 数据字典
 *
 * @author renbin
 */
@Mapper
public interface SysDictDao extends BaseMapper<SysDictEntity> {
	
}
