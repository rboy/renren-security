package com.online.edu.modules.job.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.online.edu.modules.job.entity.ScheduleJobLogEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 定时任务日志
 *
 * @author renbin
 */
@Mapper
public interface ScheduleJobLogDao extends BaseMapper<ScheduleJobLogEntity> {
	
}
