package com.online.edu.modules.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.online.edu.modules.sys.entity.SysRoleEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 角色管理
 *
 * @author renbin
 */
@Mapper
public interface SysRoleDao extends BaseMapper<SysRoleEntity> {
	

}
