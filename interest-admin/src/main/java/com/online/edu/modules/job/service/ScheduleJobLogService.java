package com.online.edu.modules.job.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.online.edu.common.utils.PageUtils;
import com.online.edu.modules.job.entity.ScheduleJobLogEntity;

import java.util.Map;

/**
 * 定时任务日志
 *
 * @author renbin
 */
public interface ScheduleJobLogService extends IService<ScheduleJobLogEntity> {

	PageUtils queryPage(Map<String, Object> params);
	
}
