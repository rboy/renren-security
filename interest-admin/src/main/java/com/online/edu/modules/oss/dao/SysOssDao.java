package com.online.edu.modules.oss.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.online.edu.modules.oss.entity.SysOssEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 文件上传
 *
 * @author renbin
 */
@Mapper
public interface SysOssDao extends BaseMapper<SysOssEntity> {
	
}
