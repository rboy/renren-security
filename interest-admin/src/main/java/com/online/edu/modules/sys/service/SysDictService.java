package com.online.edu.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.online.edu.common.utils.PageUtils;
import com.online.edu.modules.sys.entity.SysDictEntity;

import java.util.Map;

/**
 * 数据字典
 *
 * @author renbin
 */
public interface SysDictService extends IService<SysDictEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

