package com.online.edu.modules.sys.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.online.edu.common.utils.PageUtils;
import com.online.edu.modules.sys.entity.SysLogEntity;

import java.util.Map;


/**
 * 系统日志
 *
 * @author renbin
 */
public interface SysLogService extends IService<SysLogEntity> {

    PageUtils queryPage(Map<String, Object> params);

}
