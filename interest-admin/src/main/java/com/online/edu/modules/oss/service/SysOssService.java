package com.online.edu.modules.oss.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.online.edu.common.utils.PageUtils;
import com.online.edu.modules.oss.entity.SysOssEntity;

import java.util.Map;

/**
 * 文件上传
 *
 * @author renbin
 */
public interface SysOssService extends IService<SysOssEntity> {

	PageUtils queryPage(Map<String, Object> params);
}
