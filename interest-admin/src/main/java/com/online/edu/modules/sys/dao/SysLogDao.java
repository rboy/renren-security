package com.online.edu.modules.sys.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.online.edu.modules.sys.entity.SysLogEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 系统日志
 *
 * @author renbin
 */
@Mapper
public interface SysLogDao extends BaseMapper<SysLogEntity> {
	
}
