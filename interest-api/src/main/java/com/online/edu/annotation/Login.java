package com.online.edu.annotation;

import java.lang.annotation.*;

/**
 * 登录效验
 *
 * @author renbin
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Login {
}
