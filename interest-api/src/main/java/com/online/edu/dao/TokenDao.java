package com.online.edu.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.online.edu.entity.TokenEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 用户Token
 *
 * @author renbin
 */
@Mapper
public interface TokenDao extends BaseMapper<TokenEntity> {
	
}
