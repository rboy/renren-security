package com.online.edu.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.online.edu.entity.UserEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 用户
 *
 * @author renbin
 */
@Mapper
public interface UserDao extends BaseMapper<UserEntity> {

}
